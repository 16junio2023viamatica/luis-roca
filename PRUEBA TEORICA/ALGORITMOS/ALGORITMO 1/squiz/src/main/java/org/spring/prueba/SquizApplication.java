package org.spring.prueba;

import java.util.Random;

import javax.swing.JOptionPane;


public class SquizApplication {

	public static void main(String[] args) {
		
		String cadenaConcatenada="";
		
		String parametro1 = JOptionPane.showInputDialog("Ingrese la provincia: ");
		String parametro2 = JOptionPane.showInputDialog("Ingrese el tipo: ");
		Integer parametro3 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese # bloques: "));
		
		char primercaracter = parametro1.charAt(0);
		cadenaConcatenada = cadenaConcatenada + String.valueOf(primercaracter).toUpperCase();
		
		for(int x=0 ; x < parametro3; x++) {
			System.out.println(" ");
			for(int i=0 ; i < 3 ; i++) {
				String salida = "";
				String segundoCaracter = "";
				if(parametro2.equals("Vehículos comerciales") || parametro2.equals("Vehiculos comerciales")) {
					segundoCaracter= "A";
				}else {
					segundoCaracter = String.valueOf(randomChar());
				}
				Integer tercerCaracter = (int)(Math.random()*7+3);
				String cuartoCaracter = "-";
				Integer quintoCaracter = (int)(Math.random()*9+1);
				Integer sextoCaracter = (int)(Math.random()*9+1);
				Integer septimoCaracter = (int)(Math.random()*9+1);
				Integer octavoCaracter = (int)(Math.random()*9+1);
				salida = cadenaConcatenada + segundoCaracter + tercerCaracter + cuartoCaracter +
						quintoCaracter + sextoCaracter + septimoCaracter + octavoCaracter;
				System.out.println(salida);
			}
		}
	}
	
	private static char randomChar() {
        Random r = new Random();
        return (char)(r.nextInt(26) + 'A');
    }

}
