package org.spring.prueba;

import javax.swing.JOptionPane;

public class PalabraApplication {

	public static void main(String[] args) {
		
		String parametro1 = JOptionPane.showInputDialog("Ingrese una palabra: ");
		if(parametro1.length() % 2 == 0) {
			int mitad = parametro1.length()/2;
			System.out.println(mitad);
			String primerCorte = parametro1.substring(0,mitad);
			String segundoCorte = parametro1.substring(mitad);
			System.out.println(segundoCorte + primerCorte);
		}else {
			System.out.println("La palabra que ingresastes tiene una longitud impar");
		}
	}

}
